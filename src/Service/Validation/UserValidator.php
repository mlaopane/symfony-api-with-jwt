<?php

namespace App\Service\Validation;

use App\Entity\User;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserValidator
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * UserValidator constructor.
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @param User $user
     * @param string $password
     * @throws UnauthorizedHttpException
     */
    public function checkPassword(User $user, string $password)
    {
        if (!$this->encoder->isPasswordValid($user, $password)) {
            throw new UnauthorizedHttpException("Login", "Invalid credentials");
        }
    }
}