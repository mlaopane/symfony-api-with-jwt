<?php
namespace App\Service\Validation;

use App\Exception\Http\InvalidEntityException;

use App\Service\StringConverter;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Entity validator
 *
 * @author Mickaël LAO-PANE <mickael.lao-pane@wassa.fr>
 */
class EntityValidator
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var StringConverter
     */
    private $stringConverter;

    /**
     * @param ValidatorInterface $validator [description]
     * @param StringConverter $stringConverter
     */
    public function __construct(ValidatorInterface $validator, StringConverter $stringConverter)
    {
        $this->validator = $validator;
        $this->stringConverter = $stringConverter;
    }

    /**
     * Uses the Symfony validator to validate an entity
     * Throws an Exception if there are validation errors
     * The errors can be retrieved within the exception
     *
     * @param  mixed entity
     *
     * @throws InvalidEntityException
     */
    public function validate($entity, $constraints = null, $groups = null)
    {
        /**
         * @var ConstraintViolation[]
         */
        $validationErrors = $this->validator
            ->validate($entity, $constraints, $groups)
            ->getIterator()
            ->getArrayCopy()
        ;

        $errors = $this->buildErrorsFromViolations($validationErrors);

        if (count($errors) === 0) {
            return;
        }

        throw new InvalidEntityException("The provided entity is not valid", $errors);
    }

    /**
     * Build an associative array of errors
     * indexed by the snake cased field names
     * @param ConstraintViolation[] $validationErrors
     * @return array
     */
    private function buildErrorsFromViolations(array $validationErrors): array
    {
        return array_reduce($validationErrors, function ($stack, $validationError) {
            $propertyName = $validationError->getPropertyPath();
            $fieldName = $this->stringConverter->toSnakeCase($propertyName);
            $stack[$fieldName] = $validationError->getMessage();
            return $stack;
        }, []);
    }
}
