<?php

namespace App\Service\Manager;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class AbstractManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $manager;

    /**
     * UserManager constructor.
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Persist an Entity then flush
     * @param mixed $entity
     */
    protected function persistAndFlush($entity)
    {
        $this->manager->persist($entity);
        $this->manager->flush();
    }
}