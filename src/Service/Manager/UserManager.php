<?php

namespace App\Service\Manager;

use App\Entity\User;
use App\Security\Exception\PasswordGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserManager extends AbstractManager
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordGenerator;

    /**
     * UserManager constructor.
     * @param EntityManagerInterface $manager
     * @param UserPasswordEncoderInterface $encoder
     * @param PasswordGenerator $passwordGenerator
     */
    public function __construct(
        EntityManagerInterface $manager,
        UserPasswordEncoderInterface $encoder,
        PasswordGenerator $passwordGenerator
    ) {
        $this->encoder = $encoder;
        $this->passwordGenerator = $passwordGenerator;
        parent::__construct($manager);
    }

    /**
     * Takes a plain new password
     * and set anencoded password to the provided user
     * @param User $user
     * @param string $newPassword the plain password
     */
    public function updatePassword(User $user, string $newPassword)
    {
        $encodedPassword = $this->encoder->encodePassword($user, $newPassword);
        $user->setPassword($encodedPassword);
        $this->persistAndFlush($user);
    }

    /**
     * Takes a plain new password
     * and set anencoded password to the provided user
     * @param User $user
     * @return string
     * @throws \Exception
     */
    public function randomizePassword(User $user): string
    {
        $securedPassword = $this->passwordGenerator->generateSecured(8);
        $encodedPassword = $this->encoder->encodePassword($user, $securedPassword);
        $user->setPassword($encodedPassword);
        $this->persistAndFlush($user);

        return $securedPassword;
    }
}