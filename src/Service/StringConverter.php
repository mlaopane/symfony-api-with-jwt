<?php
/**
 * Created by PhpStorm.
 * User: mlao
 * Date: 03/05/2018
 * Time: 10:52
 */

namespace App\Service;


class StringConverter
{
    /**
     * @param string $camelCase
     * @return string
     */
    public function toSnakeCase(string $camelCase): string
    {
        return ltrim(strtolower(preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '_$0', $camelCase)), '_');
    }
}