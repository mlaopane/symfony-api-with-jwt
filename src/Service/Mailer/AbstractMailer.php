<?php

namespace App\Service\Mailer;

use Symfony\Component\Templating\EngineInterface;

abstract class AbstractMailer
{
    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @var EngineInterface
     */
    protected $templating;

    /**
     * AbstractMailer constructor.
     * @param \Swift_Mailer $mailer
     * @param \Twig_Environment $templating
     */
    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $templating)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    /**
     * @param string $templatePath
     * @param array $parameters
     * @return string
     */
    protected function render(string $templatePath, array $parameters = []): string
    {
        return $this->templating->render($templatePath, $parameters);
    }
}