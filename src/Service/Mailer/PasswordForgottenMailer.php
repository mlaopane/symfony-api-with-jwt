<?php

namespace App\Service\Mailer;

use App\Entity\User;

class PasswordForgottenMailer extends AbstractMailer
{
    const FROM = "able@wassa.io";

    public function send(User $user, string $plainPassword)
    {
        $message = $this->createPasswordForgottenMessage($user, $plainPassword);

        $this->mailer->send($message);
    }

    /**
     * @param User $user
     * @param string $plainPassword
     * @return \Swift_Message
     */
    private function createPasswordForgottenMessage(User $user, string $plainPassword): \Swift_Message
    {
        $message = new \Swift_Message();
        $message->setSubject("Able - Password forgotten");
        $message->setFrom(static::FROM);
        $message->setTo($user->getEmail());
        $message->setBody(
            $this->render("email/password_forgotten.html.twig", [
                "plainPassword" => $plainPassword,
            ]),
            "text/html"
        );

        return $message;
    }
}