<?php

namespace App\EventSubscriber;

use App\Exception\Http\InvalidEntityException;
use App\Model\Response\HttpExceptionResponse;
use App\Model\Response\InvalidEntityExceptionResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;

class HttpExceptionSubscriber implements EventSubscriberInterface
{
    /**
     * Set a custom response for specific HTTP exceptions
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        switch (true) {
            case $exception instanceof InvalidEntityException: {
                $event->setResponse(new InvalidEntityExceptionResponse($exception));
                break;
            }
            case $exception instanceof HttpException: {
                $event->setResponse(new HttpExceptionResponse($exception));
                break;
            }
            default:
                break;
        }
    }

    public static function getSubscribedEvents()
    {
        return [
           'kernel.exception' => 'onKernelException',
        ];
    }
}
