<?php

namespace App\Controller;

use App\Exception\Http\InvalidEntityException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

abstract class AbstractController extends Controller
{
    /**
     * @param InvalidEntityException $exception
     * @return JsonResponse
     */
    protected function invalidEntityExceptionResponse(InvalidEntityException $exception): JsonResponse
    {
        $data       = ["message" => $exception->getMessage()];
        $statusCode = $exception->getStatusCode();

        return $this->json([
            "error" => [
                "message" => $exception->getMessage(),
                "fields" => $exception->getErrors(),
            ]
        ], $statusCode);
    }
}
