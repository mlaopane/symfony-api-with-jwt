<?php

namespace App\Controller\API;

use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Controller\AbstractController as BaseController;

abstract class AbstractController extends BaseController
{
    /**
     * @return string
     */
    protected function getPixliticsToken(): string
    {
        return $this->getParameter("pixlytics.token");
    }

    protected function getJwtFromRequest(Request $request): User
    {
        $authorization = $request->headers->get("Authorization");
        return str_replace(["Bearer "], "", $authorization);
    }

    /**
     * Returns a JsonResponse that uses the jms_serializer component if enabled, or json_encode.
     *
     * @param  mixed   $data
     * @param  integer $status
     * @param  array   $headers
     * @param  array   $groups
     *
     * @return JsonResponse
     */
    protected function jms(
        $data,
        int $status = 200,
        array $headers = [],
        array $groups = ['Default']
    ): JsonResponse {
        // Returns the bare data if jms_serializer doesn't exist
        if (!$this->container->has('jms_serializer')) {
            return new JsonResponse($data, $status, $headers);
        }

        /** @var \JMS\Serializer\Serializer */
        $serializer = $this->container->get('jms_serializer');

        // Create a context and set the serialization groups
        $context = SerializationContext::create()->setGroups($groups);

        $serializedData = $serializer->serialize($data, 'json', $context);

        return new JsonResponse($serializedData, $status, $headers, true);
    }
}
