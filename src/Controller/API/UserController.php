<?php

namespace App\Controller\API;

use App\Entity\User;
use App\Form\LoginType;
use App\Form\PasswordForgottenType;
use App\Form\ResetPasswordType;
use App\Model\LoginModel;
use App\Model\PasswordForgottenModel;
use App\Model\ResetPasswordModel;
use App\Repository\UserRepository;
use App\Security\JWTGenerator;
use App\Service\Mailer\PasswordForgottenMailer;
use App\Service\Manager\UserManager;
use App\Service\Validation\EntityValidator;
use App\Service\Validation\UserValidator;

use FOS\RestBundle\Controller\Annotations as FOS;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

/**
 * @Route("/api/user", name="api_user_")
 */
class UserController extends AbstractController
{
    /**
     * @FOS\Post("/login", name="login")
     * @SWG\Post(
     *     path="/api/user/login",
     *     tags={"User"},
     *     produces={"application/json"},
     *     summary="Authentication",
     *     @SWG\Response(response=200, description="Authentication successful", @Model(type=User::class, groups={"login_response"})),
     *     @SWG\Response(response=400, description="Bad request"),
     *     @SWG\Response(response=401, description="Invalid credentials"),
     *     @SWG\Parameter(name="Credentials", in="body", required=true,
     *         @Model(type=LoginModel::class)
     *     )
     * )
     * @param Request $request
     * @param EntityValidator $validator
     * @param UserRepository $userRepository
     * @param UserValidator $userValidator
     * @param JWTGenerator $jwtGenerator
     * @return JsonResponse
     */
    public function login(
        Request $request,
        EntityValidator $validator,
        UserRepository $userRepository,
        UserValidator $userValidator,
        JWTGenerator $jwtGenerator
    ): JsonResponse {
        $loginModel = new LoginModel();

        $form = $this->createForm(LoginType::class, $loginModel);
        $form->submit($request->request->all());

        $user = $userRepository->findOneBy(["email" => $loginModel->getEmail()]);

        if (null === $user) {
            throw new UnauthorizedHttpException("Login", "Bad credentials");
        }

        $userValidator->checkPassword($user, $loginModel->getPassword());

        return $this->jms([
            "api_token" => $jwtGenerator->generateUserToken($user),
        ], 200);
    }

    /**
     * @FOS\Post("/reset-password", name="reset-password")
     * @SWG\Post(
     *     path="/api/user/reset-password",
     *     tags={"User"},
     *     produces={"application/json"},
     *     summary="Reset password",
     *     @SWG\Response(response=200, description="Reset successful"),
     *     @SWG\Response(response=400, description="Bad request"),
     *     @SWG\Response(response=401, description="Bad credentials"),
     *     @SWG\Response(response=404, description="Email not found"),
     *     @SWG\Parameter(name="Payload", in="body", required=true,
     *         @Model(type=ResetPasswordModel::class)
     *     )
     * )
     * @param Request $request
     * @param EntityValidator $validator
     * @param UserRepository $userRepository
     * @param UserManager $userManager
     * @param UserValidator $userValidator
     * @return JsonResponse
     */
    public function resetPassword(
        Request $request,
        EntityValidator $validator,
        UserRepository $userRepository,
        UserManager $userManager,
        UserValidator $userValidator
    ): JsonResponse {
        $resetPasswordModel = new ResetPasswordModel();

        $form = $this->createForm(ResetPasswordType::class, $resetPasswordModel);
        $form->submit($request->request->all());

        $validator->validate($resetPasswordModel);

        $user = $userRepository->findOneBy([
            "email" => $resetPasswordModel->getEmail()
        ]);

        if (null === $user) {
            throw new UnauthorizedHttpException("Reset password", "Bad credentials");
        }

        $userValidator->checkPassword($user, $resetPasswordModel->getOldPassword());
        $userManager->updatePassword($user, $resetPasswordModel->getNewPassword());

        return $this->json(["message" => "Password successfully reset"]);
    }

    /**
     * @FOS\Post("/password-forgotten", name="password-forgotten")
     * @SWG\Post(
     *     path="/api/user/password-forgotten",
     *     tags={"User"},
     *     produces={"application/json"},
     *     summary="Password forgotten",
     *     @SWG\Response(response=200, description="Email successfully sent"),
     *     @SWG\Response(response=400, description="Bad request"),
     *     @SWG\Response(response=404, description="Email not found"),
     *     @SWG\Parameter(name="Payload", in="body", required=true,
     *         @Model(type=PasswordForgottenModel::class)
     *     )
     * )
     * @param Request $request
     * @param EntityValidator $validator
     * @param UserRepository $userRepository
     * @param UserManager $userManager
     * @param PasswordForgottenMailer $mailer
     * @return JsonResponse
     * @throws \Exception
     */
    public function passwordForgotten(
        Request $request,
        EntityValidator $validator,
        UserRepository $userRepository,
        UserManager $userManager,
        PasswordForgottenMailer $mailer
    ): JsonResponse {
        $passwordForgottenModel = new PasswordForgottenModel;

        $form = $this->createForm(PasswordForgottenType::class, $passwordForgottenModel);
        $form->submit($request->request->all());

        $validator->validate($passwordForgottenModel);

        $user = $userRepository->findOneBy(["email" => $passwordForgottenModel->getEmail()]);

        if (null === $user) {
            throw new NotFoundHttpException("Email not found");
        }

        $randomPassword = $userManager->randomizePassword($user);

        $mailer->send($user, $randomPassword);

        return $this->json(["message" => "An email has been sent to the user : {$passwordForgottenModel->getEmail()}"]);
    }
}
