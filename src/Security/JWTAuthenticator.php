<?php

namespace App\Security;

use Firebase\JWT\JWT as JWTService;
use App\Security\Exception\JWTAuthenticatorException;

class JWTAuthenticator
{
    /**
     * @var FirebaseJWT
     */
    private $firebaseJWT = null;

    /**
     * @var string
     */
    private $secret = "";

    /**
     * @var string
     */
    private $algorithm = "HS256";

    /**
     * @var array
     */
    private $claims = [];

    /**
     * Dependency Injection
     *
     * @param FirebaseJWT $firebaseJWT
     * @param string  $secret
     * @param string  $algorithm
     * @param string  $issuer
     * @param int     $expiration
     */
    public function __construct(
        JWTService  $jwtService,
        string      $secret,
        string      $algorithm    = 'HS256',
        string      $issuer       = null,
        int         $expiration   = null
    ) {
        $this->jwtService = $jwtService;
        $this->secret     = $secret;
        $this->algorithm  = $algorithm;

        // Initialize the base claims

        $this->claims['iat'] = time();

        if (null !== $issuer) {
            $this->claims['iss'] = $issuer;
        }


        // Default expiration = 24 hours
        $this->claims['exp'] = time() + ($expiration ?? 60 * 60 * 24);
    }

    /**
     * Create a token with optional claims
     * The claims MUST be provided as associative array
     *
     * @param  array $claims
     *
     * @throws JWTAuthenticatorException
     *
     * @return string
     */
    public function create(array $claims = []): string
    {
        foreach ($claims as $key => $value) {
            $this->claims[$key] = $value;
        }

        try {
            /** @var string */
            $jwt = $this->jwtService::encode($this->claims, $this->secret, $this->algorithm);
        } catch (\Exception $exception) {
            throw new JWTAuthenticatorException($exception->getMessage());
        }

        return $jwt;
    }

    /**
     * Parse a JWT string
     * then returns the claims as an associative array
     *
     * @param  string $jwt
     *
     * @throws JWTAuthenticatorException
     *
     * @return array
     */
    public function parse(string $jwt): array
    {
        try {
            /** @var Object */
            $payload = $this->jwtService::decode($jwt, $this->secret, [$this->algorithm]);
        } catch (\Exception $exception) {
            throw new JWTAuthenticatorException($exception->getMessage());
        }

        return (array) $payload;
    }
}