<?php

namespace App\Security;

use App\Entity\User;

class JWTGenerator
{
    /**
     * @var JWTAuthenticator
     */
    private $authenticator;

    /**
     * JWTGenerator constructor.
     * @param JWTAuthenticator $authenticator
     */
    public function __construct(JWTAuthenticator $authenticator)
    {
        $this->authenticator = $authenticator;
    }

    /**
     * @param User $user
     * @return string
     */
    public function generateUserToken(User $user): string
    {
        return $this->authenticator->create([
            "email"     => $user->getEmail(),
            "firstname" => $user->getFirstname(),
            "lastname"  => $user->getLastname(),
            "roles"     => implode(",", $user->getRoles()),
        ]);
    }
}