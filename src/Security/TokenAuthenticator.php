<?php

namespace App\Security;

use App\Security\Exception\JWTAuthenticatorException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class TokenAuthenticator extends AbstractGuardAuthenticator
{
    /**
     * @var JWTAuthenticator
     */
    private $jwtAuthenticator;

    /**
     * TokenAuthenticator constructor.
     * @param JWTAuthenticator $jwtAuthenticator
     */
    public function __construct(JWTAuthenticator $jwtAuthenticator)
    {
        $this->jwtAuthenticator = $jwtAuthenticator;
    }

    /**
     * Called on every request to decide if this authenticator should be used for the request.
     * Returning false will cause this authenticator to be skipped.
     * @param Request $request
     * @return bool
     */
    public function supports(Request $request)
    {
        return true;
    }

    /**
     * Called on every request. Return whatever credentials you want to
     * be passed to getUser() as $credentials.
     *
     * @param Request $request
     * @return string
     */
    public function getCredentials(Request $request)
    {
        $authorization = $request->headers->get('Authorization');
        $jwt = str_replace(["Bearer "], "", $authorization);

        return (string) $jwt;
    }

    /**
     * @param string $jwt
     * @param UserProviderInterface $userProvider
     * @return null|UserInterface
     */
    public function getUser($jwt, UserProviderInterface $userProvider)
    {
        // Parse and verify the token
        try {
            $payload = $this->jwtAuthenticator->parse($jwt);
        } catch (JWTAuthenticatorException $exception) {
            throw new UnauthorizedHttpException("Bearer", "Unauthorized access => Wrong token");
        }

        // Check if the email claim does exist
        if (!isset($payload["email"])) {
            throw new UnauthorizedHttpException("Bearer", "Unauthorized access => Wrong token");
        }

        return $userProvider->loadUserByUsername($payload["email"]);
    }

    /**
     * @param mixed $credentials
     * @param UserInterface $user
     * @return bool
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        // return true to cause authentication success
        return true;
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey
     * @return null|Response
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // on success, let the request continue
        return null;
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     * @return null|JsonResponse|Response
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = array(
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())
        );

        return new JsonResponse($data, Response::HTTP_FORBIDDEN);
    }

    /**
     * Called when authentication is needed, but it's not sent
     * @param Request $request
     * @param AuthenticationException|null $authException
     * @return JsonResponse
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = array(
            'message' => 'Authentication Required'
        );

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function supportsRememberMe()
    {
        return false;
    }
}