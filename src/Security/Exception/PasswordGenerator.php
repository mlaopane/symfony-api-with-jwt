<?php

namespace App\Security\Exception;

use App\Entity\User;

class PasswordGenerator
{
    /**
     * @param int $length
     * @param string $keyspace
     * @return string
     * @throws \Exception
     */
    public function generateSecured(
        int $length = 8,
        string $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    ): string {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        if ($max < 1) {
            throw new \Exception('$keyspace must be at least two characters long');
        }
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        return $str;
    }
}