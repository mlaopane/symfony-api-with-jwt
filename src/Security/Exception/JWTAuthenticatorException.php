<?php

namespace App\Security\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

class JWTAuthenticatorException extends HttpException
{
    public function __construct(
        string $message,
        \Exception $previous = null,
        array $headers = [],
        int $code = 0
    ) {
        parent::__construct(400, $message, $previous, $headers, $code);
    }
}