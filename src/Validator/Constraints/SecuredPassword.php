<?php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class SecuredPassword extends Constraint
{
    public $message = 'Invalid format : "{{ password }}"';
}