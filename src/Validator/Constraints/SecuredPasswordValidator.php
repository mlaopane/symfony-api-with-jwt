<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class SecuredPasswordValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        /**
         * At least :
         * - 1 upper case letter
         * - 1 lower case letter
         * - 1 number
         * - 1 special character
         * - 8 characters long
         */
        $validPattern = "/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-_]).{8,}$/";
        if (!preg_match($validPattern, $value, $matches)) {
            $this->context->buildViolation($constraint->message)
            ->setParameter('{{ password }}', $value)
            ->addViolation();
        }
    }
}