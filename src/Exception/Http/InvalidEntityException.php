<?php

namespace App\Exception\Http;

use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @author Mickaël LAO-PANE <mickael.lao-pane@wassa.fr>
 */
class InvalidEntityException extends HttpException
{
    /**
     * @var string[]
     */
    protected $errors = [];

    /**
     * RequestException constructor
     *
     * @param string[]     $errors
     * @param string       $message
     * @param \Exception   $previous
     * @param array        $headers
     * @param int          $code
     */
    public function __construct(
        string     $message = "Invalid entity",
        array      $errors = [],
        \Exception $previous = null,
        array      $headers = [],
        int        $code = 0
    ) {
        $this->errors = $errors;

        parent::__construct(400, $message, $previous, $headers, $code);
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
}
