<?php

namespace App\Model\Response;

use App\Exception\Http\InvalidEntityException;
use Symfony\Component\HttpFoundation\JsonResponse;

class InvalidEntityExceptionResponse extends JsonResponse
{
    /**
     * @param InvalidEntityException $exception
     */
    public function __construct(InvalidEntityException $exception)
    {
        $data = [
            "error" => [
                "message" => $exception->getMessage(),
                "fields" => $exception->getErrors(),
            ]
        ];
        $statusCode = $exception->getStatusCode();

        parent::__construct($data, $statusCode);
    }
}
