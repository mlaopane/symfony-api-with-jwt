<?php

namespace App\Model\Response;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;

class HttpExceptionResponse extends JsonResponse
{
    /**
     * @param HttpException $exception
     */
    public function __construct(HttpException $exception)
    {
        $data = [
            "error" => [
                "message" => $exception->getMessage(),
            ],
        ];
        $statusCode = $exception->getStatusCode();

        parent::__construct($data, $statusCode);
    }
}
