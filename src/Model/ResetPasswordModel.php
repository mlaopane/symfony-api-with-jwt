<?php

namespace App\Model;

use App\Validator\Constraints as CustomAssert;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

class ResetPasswordModel
{
    /**
     * @var string
     * @Assert\Email(message="Invalid format : {{ value }}")
     * @JMS\Type("string")
     */
    private $email;

    /**
     * @var string
     * @CustomAssert\SecuredPassword
     * @JMS\Type("string")
     */
    private $oldPassword;

    /**
     * @var string
     * @CustomAssert\SecuredPassword
     * @JMS\Type("string")
     */
    private $newPassword;

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getOldPassword(): ?string
    {
        return $this->oldPassword;
    }

    /**
     * @param string $oldPassword
     */
    public function setOldPassword(string $oldPassword): void
    {
        $this->oldPassword = $oldPassword;
    }

    /**
     * @return string
     */
    public function getNewPassword(): ?string
    {
        return $this->newPassword;
    }

    /**
     * @param string $newPassword
     */
    public function setNewPassword(string $newPassword): void
    {
        $this->newPassword = $newPassword;
    }
}