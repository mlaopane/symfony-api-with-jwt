<?php

namespace App\Model;

use App\Validator\Constraints as CustomAssert;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

class PasswordForgottenModel
{
    /**
     * @var string
     * @Assert\Email(message="Invalid format : {{ value }}")
     * @JMS\Type("string")
     */
    private $email;

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }
}