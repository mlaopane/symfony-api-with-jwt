<?php

namespace App\Model;

use App\Validator\Constraints as CustomAssert;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

class LoginModel
{
    /**
     * @var string
     * @Assert\Email(message="Invalid format : {{ value }}")
     * @JMS\Type("string")
     */
    private $email;

    /**
     * @var string
     * @CustomAssert\SecuredPassword
     * @JMS\Type("string")
     */
    private $password;

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return LoginModel
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return LoginModel
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
}
